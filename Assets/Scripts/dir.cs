using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dir : MonoBehaviour
{

    Vector3 startPosition, currentPosition, direction, mouseEnd, mouseStart, rayOrigin, playerVelocity;
    float maxDistance = 0.9f;
    public float throwForce = 2f;
    public Camera playerCamera;

    void funcUpdate()
    {
        // star
        if (Input.GetMouseButtonDown(0) || (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began))
        {
#if UNITY_EDITOR
            startPosition = Input.mousePosition;
#elif UNITY_IOS
                startPosition = Input.GetTouch (0).position;
#endif
            startPosition.z = playerCamera.transform.position.y - transform.position.y;
            mouseStart = playerCamera.ScreenToWorldPoint(startPosition);

            // lineRenderer.positionCount = 1;
            // lineRenderer.SetPosition(0, GetPlayerPosition());
            // lineRenderer.enabled = true;
        }

        // hold
        if (Input.GetMouseButton(0) || (Input.touchCount == 1))
        {
#if UNITY_EDITOR
            currentPosition = Input.mousePosition;
#elif UNITY_IOS
            currentPosition = Input.GetTouch (0).position;
#endif
            currentPosition.z = playerCamera.transform.position.y - transform.position.y;
            mouseEnd = playerCamera.ScreenToWorldPoint(currentPosition);
            direction = mouseStart - mouseEnd;
            direction.Normalize();

            float x = Mathf.Pow(mouseEnd.x - mouseStart.x, 2);
            float y = Mathf.Pow(mouseEnd.z - mouseStart.z, 2);
            float dirFloat = Mathf.Sqrt(x + y);

            if (dirFloat > 0.1f)
            {
                if (dirFloat >= maxDistance) { dirFloat = maxDistance; }
                else if (dirFloat <= -maxDistance) { dirFloat = -maxDistance; }
                Vector3 mouseDEnd = mouseStart - (direction * dirFloat / 2);

            }

            float endForce = dirFloat * throwForce;

        }

        if (Input.GetMouseButtonUp(0) || (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Ended))
        {

            float x = Mathf.Pow(mouseEnd.x - mouseStart.x, 2);
            float y = Mathf.Pow(mouseEnd.z - mouseStart.z, 2);
            float dirFloat = Mathf.Sqrt(x + y);

            if (dirFloat > 0.1f)
            {
                if (dirFloat >= maxDistance) { dirFloat = maxDistance; }
                else if (dirFloat <= -maxDistance) { dirFloat = -maxDistance; }

                float endForce = dirFloat * throwForce;
                mouseEnd = currentPosition;
                playerVelocity = direction.normalized * endForce;

            }
        }

    }
}