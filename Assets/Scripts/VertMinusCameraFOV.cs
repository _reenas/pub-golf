﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/**
 * This class attempts to force VERT- Field of view scaling.
 * By default, Unity uses the HOR+ technique.
 * 
 * http://en.wikipedia.org/wiki/Field_of_view_in_video_games#Scaling_methods
 */

[ExecuteInEditMode]
[RequireComponent (typeof (Camera))]
public class VertMinusCameraFOV : MonoBehaviour
{

    public float designTimeVerticalFieldOfView = 60;
    public int designTimeWidth = 1280;
    public int designTimeHeight = 720;
    // public CanvasScaler scaler;
    public Camera menuCamera;
    // public Vector2 designTimeAspectRatio = new Vector2(9,16);

    private float hFOVInRads;

    private int prevWidth;
    private int prevHeight;

    void Start ()
    {

        prevWidth = designTimeWidth;
        prevHeight = designTimeHeight;

        float aspectRatio = (float) designTimeWidth / (float) designTimeHeight;
        float vFOVInRads = designTimeVerticalFieldOfView * Mathf.Deg2Rad;
        hFOVInRads = 2f * Mathf.Atan (Mathf.Tan (vFOVInRads / 2f) * aspectRatio);

    }

    void Update ()
    {

        if (Screen.width != prevWidth || Screen.height != prevHeight)
        {
            // scaler.referenceResolution = new Vector2(Screen.height, Screen.width);
            float aspectRatio = (float) Screen.width / (float) Screen.height;

            float vFOVInRads = 2f * Mathf.Atan (Mathf.Tan (hFOVInRads / 2f) / aspectRatio);

            // Debug.Log ("Screen resolution change. Recomputing aspect ratio (" + aspectRatio + ") and field of view (" + vFOVInRads * Mathf.Rad2Deg + ")");

            // foreach (Camera cam in GameObject.FindObjectsOfType(typeof(Camera))) {
                float maxVFOV = 60;
                if(vFOVInRads * Mathf.Rad2Deg > 63) {
                    maxVFOV = 63;
                }
                else if(vFOVInRads * Mathf.Rad2Deg < 52) {
                    maxVFOV = 52;
                }
                else {
                    maxVFOV = vFOVInRads * Mathf.Rad2Deg;
                }
            menuCamera.fieldOfView = Mathf.Round (maxVFOV);
            // }
        }

    }

}
/* using System.Collections;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent (typeof (Camera))]
public class VertMinusCameraFOV : MonoBehaviour
{
    //Set a fixed horizontal FOV
    public float horizontalFOV = 35f;
    public float designTimeVerticalFieldOfView = 60;
    public int designTimeWidth = 720;
    public int designTimeHeight = 1280;

    private float hFOVInRads;

    private int prevWidth;
    private int prevHeight;

    public Camera _camera;

    void Start ()
    {

        // prevWidth = designTimeWidth;
        // prevHeight = designTimeHeight;

        // float aspectRatio = (float) designTimeWidth / (float) designTimeHeight;
        // float vFOVInRads = designTimeVerticalFieldOfView * Mathf.Deg2Rad;
        // hFOVInRads = 2f * Mathf.Atan (Mathf.Tan (vFOVInRads / 2f) * aspectRatio);

    }

    void Update ()
    {
        float halfWidth = Mathf.Tan (0.5f * horizontalFOV * Mathf.Deg2Rad);

        float halfHeight = halfWidth * Screen.height / Screen.width;

        float verticalFoV = 2.0f * Mathf.Atan (halfHeight) * Mathf.Rad2Deg;

        _camera.fieldOfView = verticalFoV;

        float frustumHeight = 2.0f * distance * Mathf.Tan (camera.fieldOfView * 0.5f * Mathf.Deg2Rad);
        var frustumWidth = frustumHeight * camera.aspect;
        var frustumHeight = frustumWidth / camera.aspect;

        // _camera.aspect = 9f/16;
        // if (Screen.width != prevWidth || Screen.height != prevHeight)
        // {

        //     float aspectRatio = (float) Screen.width / (float) Screen.height;

        //     float vFOVInRads = 2f * Mathf.Atan (Mathf.Tan (hFOVInRads / 2f) / aspectRatio);

        //     Debug.Log ("Screen resolution change. Recomputing aspect ratio (" + aspectRatio + ") and field of view (" + vFOVInRads * Mathf.Rad2Deg + ")");

        //     foreach (Camera cam in GameObject.FindObjectsOfType (typeof (Camera)))
        //     {
        //         cam.fieldOfView = vFOVInRads * Mathf.Rad2Deg;
        //     }
        // }

        // _camera.fieldOfView = calcVertivalFOV (horizontalFOV, _camera.aspect);
        // Standard film size
        // int filmHeight = 72;
        // int filmWidth = 46;

        // // Formula to convert focalLength to field of view - In Unity they use Vertical FOV.
        // // So we use the filmHeight to calculate Vertical FOV.
        // double fovdub = Mathf.Rad2Deg * 2.0 * Mathf.Atan (filmHeight / (2.0f * 60.0f));
        // float fov = (float) fovdub;
        // _camera.fieldOfView = fov;

    }

    private float calcVertivalFOV (float hFOVInDeg, float aspectRatio)
    {
        float hFOVInRads = hFOVInDeg * Mathf.Deg2Rad;
        float vFOVInRads = 2 * Mathf.Atan (Mathf.Tan (hFOVInRads / 2) / aspectRatio);
        float vFOV = vFOVInRads * Mathf.Rad2Deg;
        return vFOV;
    }

}
*/