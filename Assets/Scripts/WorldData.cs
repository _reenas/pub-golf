﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "WorldData")]
[System.Serializable]
public class WorldData : ScriptableObject
{
    public int worldId = 0;
    public string wordName;
    [SerializeField]
    public List<LevelData> levels;
    public bool isUnlocked;
    public Color worldColor;

    void OnEnable()
    {
        worldId = 4;
    }
}