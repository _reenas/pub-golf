﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    public LevelData levelData;

    void OnEnable()
    {
        // levelData.levelPrefab = this.gameObject;
        SetStartEndPoint(levelData);
    }

    void SetStartEndPoint(LevelData levelToSet)
    {
        for (int child = 0; child < transform.childCount; child++)
        {
            if (transform.GetChild(child).name == "Start")
            {
                levelToSet.startPoint = transform.GetChild(child);
            }
            if (transform.GetChild(child).name == "End")
            {
                levelToSet.endPoint = transform.GetChild(child);
            }
            if (levelToSet.startPoint != null && levelToSet.endPoint != null)
            {
                // Debug.Log("did break");
                break;
            }
        }
    }
}
