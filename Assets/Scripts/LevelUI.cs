﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelUI : MonoBehaviour
{
    private Color gotStar = new Color(1, 1, 1, 1f);
    private Color noStar = new Color(0f, 0f, 0f, 1f);

    public Image baseImage;

    // Start is called before the first frame update
    void Awake()
    {
        // baseImage = GetComponent<Image>();
    }

    public void Init(string _name, int _starCount, Color _worldColor)
    {
        SetLevelPanelStars(_starCount);
        baseImage.color = _worldColor;
        transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = _name;
    }

    public void SetLevelPanelStars(int _starCount)
    {
        // Set all stars to empty
        for (int child = 0; child < transform.GetChild(1).childCount; child++)
        {
            ChangeSpriteColor(transform.GetChild(1).GetChild(child).gameObject, false);
        }
        // Set stars equal tu current star count
        for (int star = 0; star < _starCount; star++)
        {
            ChangeSpriteColor(transform.GetChild(1).GetChild(star).gameObject, true);
        }
    }

    public void ChangeSpriteColor(GameObject _changeable, bool _change)
    {
        _changeable.GetComponent<Image>().color = (_change ? gotStar : noStar);
    }
}
