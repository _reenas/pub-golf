﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Coin : ScriptableObject
{
    public int id;
    public UnityEngine.UI.Image image;
    public Sprite sprite;
    [Multiline]public string description;
}
