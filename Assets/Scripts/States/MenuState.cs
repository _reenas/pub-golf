﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MenuState : State
{
    public override string GetName() { return "Menu"; }

    public Canvas menuCanvas;
    public Camera menuCamera;
    public Button startGameButton;

    // PANElS
    [Header("Panels")]
    public GameObject[] panelContainer;
    public PANElS activePanel = PANElS.MENU;
    private GameObject activePanelObject;

    // public GameObject mainPanel;
    // public GameObject levelsPanel;
    // public GameObject settingsPanel;
    public GameObject levelTransitionPanel;

    public enum PANElS
    {
        MENU,
        LEVELS,
        SKINS,
        MULTIPLAYER,
        SETTINGS
    };

    [Header("Settings Panel Stuff")]
    // Settings stuff
    public TextMeshProUGUI qualityText;
    public TextMeshProUGUI volumeText;
    public TextMeshProUGUI nextLevelText;
    public TextMeshProUGUI nextWorldText;
    public Toggle volumeToggle;

    [Header("Levels Panel Stuff")]
    public GameObject levelUIPrefab;
    public GameObject worldUIPrefab;
    public RectTransform worldsContainer;
    public RectTransform worldsButtons;

    private bool levelsPopulated = false;


    int volume = 10;
    int oldVolume = 0;

    #region STATE MANGER
    // Enters menu state
    public override void Enter(State from)
    {
        // This only works when game is first opened after instalation
        if (GameData.s_Version == 0)
        {
            // TODO: Create settings etc.
            volumeText.text = ((int)(AudioListener.volume * 10)).ToString();
            qualityText.text = GetCurrentQualityName();
        }

        PopulateLevels();

        // TODO: ????
        // if (LevelManager.GetCurrentLevel().x != 0 && LevelManager.GetCurrentLevel().y != 0)
        //     startGameText.text = "Continue Game";

        ChangePanels(); // Sets active panel
        menuCanvas.gameObject.SetActive(true);
        menuCamera.gameObject.SetActive(true);
    }

    // Exit menu state
    public override void Exit(State to)
    {
        menuCamera.gameObject.SetActive(false);
        menuCanvas.gameObject.SetActive(false);
    }
    public override void Tick()
    {

    }

    #endregion

    #region UTILITIES

    public void StartGame()
    {
        LevelManager.instance.SelectCurrentLevel();
        LevelManager.instance.UpdateNames();
        GameManager.instance.m_StateManager.SwitchState("Game");
    }

    public void ChangePanels(int _input = 0)
    {
        if (activePanelObject != null) activePanelObject.SetActive(false); // TODO: Do transition

        switch (_input)
        {
            case (int)PANElS.MENU:
                // Debug.Log("Menu Panel");
                activePanel = PANElS.MENU;
                activePanelObject = panelContainer[0];
                break;
            case (int)PANElS.LEVELS:
                // Debug.Log("Levels Panel");
                activePanel = PANElS.LEVELS;
                activePanelObject = panelContainer[1];
                break;
            case (int)PANElS.SKINS:
                // Debug.Log("Settings Panel");
                activePanel = PANElS.SETTINGS;
                activePanelObject = panelContainer[2];
                break;
            case (int)PANElS.MULTIPLAYER:
                // Debug.Log("Settings Panel");
                activePanel = PANElS.SETTINGS;
                activePanelObject = panelContainer[2];
                break;
            case (int)PANElS.SETTINGS:
                // Debug.Log("Settings Panel");
                activePanel = PANElS.SETTINGS;
                activePanelObject = panelContainer[2];
                break;
            default:
                Debug.Log("Default case");
                activePanel = PANElS.MENU;
                activePanelObject = panelContainer[0];
                break;
        }
        activePanelObject.SetActive(true); // TODO: Do transition
    }

    private void CreateLevelPanels(LevelData _level, int _worldID, int _levelID)
    {
        GameObject UILevelPanel = Instantiate(levelUIPrefab, worldsContainer.GetChild(_worldID).transform);
        UILevelPanel.GetComponent<LevelUI>().Init("Level " + _levelID, _level.currentStars, LevelManager.s_worlds[_worldID].worldColor);
        UILevelPanel.GetComponent<Button>().interactable = _level.isUnlocked;
        UILevelPanel.GetComponent<Button>().onClick.RemoveAllListeners();
        UILevelPanel.GetComponent<Button>().onClick.AddListener(() =>
        {
            LevelManager.instance.SetLevel(_worldID, _levelID);
            LevelManager.instance.UpdateNames();
            ChangeGameState();
        });
    }

    public void ChangeGameState()
    {
        GameManager.instance.m_StateManager.SwitchState("Game");
    }

    private void CreateWorldButtons(int _worldID)
    {
        GameObject UIWorldButton = Instantiate(worldUIPrefab, worldsButtons.transform);
        UIWorldButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = LevelManager.s_worlds[_worldID].name;
    }

    public void PopulateLevels()
    {
        if (!levelsPopulated)
        {
            
            // Debug.Log(LevelManager.s_worlds.Count);

            for (int worldID = 0; worldID < LevelManager.s_worlds.Count; worldID++)
            {
                CreateWorldButtons(worldID);

                for (int levelID = 0; levelID < LevelManager.s_worlds[worldID].levels.Count; levelID++)
                {
                    LevelData tempLevel = LevelManager.s_worlds[worldID].levels[levelID];
                    CreateLevelPanels(tempLevel, worldID, levelID);
                }
            }
        }
        levelsPopulated = true;
    }

    // public void SetName()
    // {
    //     nextLevelText.text = LevelManager.instance.levelName;
    //     nextWorldText.text = LevelManager.instance.worldName;
    // }

    public void OnWorldButtonClicked(int _worldId)
    {
        for (int i = 0; i < worldsContainer.childCount; i++)
        {
            worldsContainer.GetChild(i).gameObject.SetActive(false);
        }
        worldsContainer.GetChild(_worldId).gameObject.SetActive(true);
    }

    public void AdjustLevelCellSize(GridLayoutGroup _worldPanelGrid, RectTransform _container)
    {
        // TODO: Add spaceing and padding calc
        for (int i = 0; i < _container.childCount; i++)
        {
            GridLayoutGroup tempWorldPanel = _container.GetChild(i).GetComponent<GridLayoutGroup>();
            float width = _container.rect.width - 40;
            float height = _container.rect.height;
            float sizePerCell = width / tempWorldPanel.constraintCount;
            tempWorldPanel.cellSize = new Vector2(sizePerCell, height / 5);
        }
    }

    #endregion

    #region OPTIONS

    // TODO: save settings
    private void ChangeQuality(int _direction)
    {
        if (_direction < 0)
        {
            QualitySettings.DecreaseLevel(false);
        }
        else if (_direction > 0)
        {
            QualitySettings.IncreaseLevel(false);
        }
        qualityText.text = GetCurrentQualityName();
    }

    // TODO: Get master volume and save settings
    private void ChangeSound(int _direction)
    {
        int newVolume = volume;
        if (volume + _direction >= 10)
        {
            newVolume = 10;
        }
        else if (volume + _direction <= 0)
        {
            newVolume = 0;
        }
        else
        {
            newVolume = volume + _direction;
        }
        AudioListener.volume = (float)newVolume / 10;
        volume = newVolume;
        volumeText.text = volume.ToString();

    }

    public void ToggleVolume(Toggle change)
    {
        // TODO: For mute option ad last saved sound
        if (volumeToggle.isOn)
        {
            int temp = volume;
            volume = oldVolume;
            oldVolume = temp;
            volumeText.text = volume.ToString();
        }
        else
        {
            volume = 0;
            volumeText.text = volume.ToString();
        }
    }

    string GetCurrentQualityName()
    {
        string[] names = QualitySettings.names;
        int current_quality_id = QualitySettings.GetQualityLevel();
        return names[current_quality_id];
    }

    #endregion // OPTIONS

    #region ANIMATIONS

    // public void OpenLevels()
    // {
    //     menuEnvAnimator.SetTrigger("LevelsSelected");
    // }

    // public void SlideInLevels()
    // {
    //     menuAnimator.SetTrigger("SlideInLevels");
    // }

    // public void SlideBackLevels()
    // {
    //     menuAnimator.SetTrigger("SlideBackLevels");
    // }

    // public void OpenSettings()
    // {
    //     menuEnvAnimator.SetTrigger("SettingsSelected");
    // }

    // public void SlideInSettings()
    // {
    //     menuAnimator.SetTrigger("SlideInSettings");
    // }

    // public void SlideBackSettings()
    // {
    //     menuAnimator.SetTrigger("SlideBackSettings");
    // }

    #endregion
}