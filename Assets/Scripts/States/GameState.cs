﻿
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameState : State
{

    public Canvas gameCanvas;
    public Camera gameCamera;
    public LevelManager l_manager;
    public GameObject enter;

    public TextMeshProUGUI timerText;
    public TextMeshProUGUI counterText;
    public TextMeshProUGUI levelText;
    public TextMeshProUGUI goalText;

    public Image levelBorder;

    private float secondsCount = 0;
    private int minuteCount = 0;


    public bool cameraMoving = false;
    // public bool changeGoal = false;

    LevelData currentArea;
    public int moveCount = 0;


    public string passedTime { get; private set; }
    public bool restart { get; private set; }
    public int restartCount { get; private set; }


    public int maxRestart = 0;

    public override string GetName()
    {
        return "Game";
    }

    public override void Enter(State from)
    {
        restartCount = 0;
        

        levelText.text = "World " + LevelManager.worldID + " Level " + LevelManager.levelID;
        levelBorder.color = LevelManager.s_worlds[LevelManager.worldID].worldColor;

        LevelManager.instance.LoadLevel();
        gameCamera.gameObject.SetActive(true);
        minuteCount = 0;
        secondsCount = 0f;
        currentArea = LevelManager.s_currentLevel;
        goalText.text = "Goal: " + currentArea.stars.THREE.ToString();

        gameCanvas.gameObject.SetActive(true);
    }

    public override void Exit(State to)
    {
        // hourCount = minuteCount = 0;
        // secondsCount = 0f;
        restartCount = 0;
        gameCanvas.gameObject.SetActive(false);
        passedTime = timerText.text;
        LevelManager.instance.UnloadLevel();
        gameCamera.gameObject.SetActive(false);
    }

    public override void Tick()
    {

        if (Time.frameCount % 1 == 0)
        {
            UpdateTimerUI();
        }

        ChangeGoalText(); // Create this as event
    }

    private void ChangeGoalText()
    {
        // TODO: Add on move if
        moveCount = VARS.moveCount; // do we need this here
        counterText.text = "Moves: " + moveCount.ToString();

        if (moveCount >= 0 && currentArea.stars.THREE > moveCount)
        {
            goalText.text = "Three star goal: " + currentArea.stars.THREE.ToString();
        }
        else if (currentArea.stars.THREE <= moveCount && currentArea.stars.TWO > moveCount)
        {
            goalText.text = "Two star goal: " + currentArea.stars.TWO.ToString();
        }
        else if (currentArea.stars.TWO <= moveCount && currentArea.stars.ONE > moveCount)
        {
            goalText.text = "One star goal: " + currentArea.stars.ONE.ToString();
        }
        else if (moveCount >= currentArea.stars.ONE)
        {
            goalText.text = "You can restart";
        }
    }

    public void UpdateTimerUI()
    {
        secondsCount += Time.deltaTime;

        if (secondsCount >= 60)
        {
            minuteCount++;
            secondsCount = 0;
        }
        else if (minuteCount >= 60)
        {
            minuteCount = 0;
        }
        timerText.text =
            minuteCount.ToString("00") + ":" + secondsCount.ToString("00");
        // timerText.text = String.Format(" {0} : {1} ", minuteCount, (int)secondsCount);
    }

    public void Restart()
    {

        LevelManager.instance.ReloadLevel();
    }
}