﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameOverState : State
{
    public Canvas gameoverCanvas;
    public Animator gameOverAnimator;
    public Animator buttonsAnimator;
    private int endScore = 0;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI timeText;
    public TextMeshProUGUI levelText;
    public Camera menuCamera;

    public GameObject unlockPanel;

    public Button nextButton;
    public GameObject starPanel;
    public int stars = 0;
    bool coinUnlocked = false;

    private Color gotStar = new Color(1, 1, 1, 1f);
    private Color noStar = new Color(0f, 0f, 0f, 1f);

    public override void Enter(State from)
    {
        coinUnlocked = false;
        // starts = 1;
        menuCamera.gameObject.SetActive(true);
        // unlockPanel.SetActive (false);
        LevelData area = LevelManager.s_currentLevel;
        endScore = VARS.moveCount;
        levelText.text = LevelManager.s_currentLevel.levelPrefab.name;
        // scoreText.text = endScore.ToString ();
        timeText.text = GameData.passedTime;

        for (int i = 0; i < starPanel.transform.childCount; i++)
        {
            // ChangeSpriteColor (starPanel.transform.GetChild (i).gameObject, false);

        }

        if (endScore <= 0 && endScore > area.stars.ONE)
        {
            scoreText.text = "You so bad";
            LevelManager.instance.CheckHighScore(0);
            nextButton.interactable = false;
            stars = 0;
        }
        else if (endScore > 0 && endScore <= area.stars.THREE)
        {
            LevelManager.instance.CheckHighScore(3);
            scoreText.text = "You di man";
            stars = 3;
            for (int i = 0; i < starPanel.transform.childCount; i++)
            {
                // ChangeSpriteColor (starPanel.transform.GetChild (i).gameObject, true);
                unlockPanel.transform.GetChild(1).GetComponent<Image>().sprite = area.coin.sprite;
                // unlockPanel.SetActive (true);
                coinUnlocked = true;
            }
        }
        else if (endScore > area.stars.THREE && endScore <= area.stars.TWO)
        {
            LevelManager.instance.CheckHighScore(2);
            scoreText.text = "Ain't bad";
            stars = 2;
            for (int i = 0; i < 2; i++)
            {
                // ChangeSpriteColor (starPanel.transform.GetChild (i).gameObject, true);
            }
        }
        else if (endScore > area.stars.TWO && endScore <= area.stars.ONE)
        {
            LevelManager.instance.CheckHighScore(1);
            scoreText.text = "Try better boi";
            stars = 1;
            // ChangeSpriteColor (starPanel.transform.GetChild (0).gameObject, true);
        }

        if (endScore <= area.stars.ONE && endScore > 0)
        {
            nextButton.interactable = true;
            Vector2 levelIndex = LevelManager.instance.GetLevelIndex(area);
            LevelManager.instance.UnlockNextLevel(area);
        }
        else
        {
            nextButton.interactable = false;
        }

        // And enable canvas
        ActivateCanvas();
        // gameOverAnimator.SetTrigger ("Start Game Over");

        // Debug.Break();
        // PlayerData.instance.Save ();
    }

    public void ActivateCanvas()
    {
        gameoverCanvas.gameObject.SetActive(true);
    }

    public override void Exit(State to)
    {
        gameoverCanvas.gameObject.SetActive(false);
        menuCamera.gameObject.SetActive(false);
    }

    public override void Tick()
    {

    }

    public override string GetName()
    {
        return "GameOver";
    }

    public void MainMenu()
    {
        GameManager.instance.m_StateManager.SwitchState("Menu");
    }

    public void Restart()
    {
        GameManager.instance.m_StateManager.SwitchState("Game");
    }

    public void NextLevel()
    {
        // StartCoroutine (UnityAds.instance.ShowAd ());

        if (LevelManager.instance.NextLevel())
        {
            Restart();
            // AdMob.instance.ShowAdd();
        }
        else
        {
            MainMenu();
        }
    }

    public void ChangeSpriteColor(GameObject _changeable, bool _change)
    {
        _changeable.GetComponent<Image>().color = (_change ? gotStar : noStar);
    }

    public void CheckStars()
    {
        if (stars == 1)
        {
            gameOverAnimator.SetBool("One Star", true);
        }
        else if (stars == 2)
        {
            gameOverAnimator.SetBool("Two Stars", true);
        }
        else if (stars == 3)
        {
            gameOverAnimator.SetBool("Three Stars", true);
        }
        else if (stars == 0)
        {
            gameOverAnimator.SetBool("No Star", true);
        }
    }

    public void ShowButtons()
    {
        buttonsAnimator.SetTrigger("Show Buttons");
    }

}