﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class LevelData : ScriptableObject
{
    public string name = "";
    public GameObject levelPrefab;
    public Coin coin;

    public VARS.STARS stars;

    public bool isUnlocked = false;

    public int currentStars = 0;

    [HideInInspector] public Transform startPoint;
    [HideInInspector] public Transform endPoint;

    void SetCurrentStars()
    {
        // TODO: how we will save and load done levels??
        // currentStars = SaveData.level[i].stars
    }

    void UnlockLevel()
    {
        this.isUnlocked = true;
    }

    // LevelSaveData DataToSave()
    // {
    //     return new LevelSaveData(this);
    // }
}