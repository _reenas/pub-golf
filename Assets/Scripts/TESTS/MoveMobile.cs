﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveMobile : MonoBehaviour
{
    // Use this for initialization
    private Vector2 start;
    RectTransform rt;
    public Camera _camera;
    // private float scaler;
    // private int width;
    // private int height;
    // public CanvasScaler canvasScaler;

    void Start ()
    {
        // height = Screen.height;
        // width = Screen.width;

        // // scaler = GetScale (height, width, new Vector2 (1280, 720), 0.5f);
        // scaler = GetScale(height, width, canvasScaler);
        // Debug.Log (scaler);
        // rt = GetComponent<RectTransform> ();
        if (_camera == null)
            _camera = Camera.main;
        // ensure calculations are done when the camera is not rotated. Otherwise the z-axis will incorrectly have some depth
        Vector3 camRotation = _camera.transform.rotation.eulerAngles;
        _camera.transform.rotation = Quaternion.Euler (Vector3.zero);
        // find corners of the cameras view frustrum at the distance of the gameobject
        float distance = Vector3.Distance (this.transform.position, _camera.transform.position);
        Vector3 viewBottomLeft = _camera.ViewportToWorldPoint (new Vector3 (0, 0, distance));
        Vector3 viewTopRight = _camera.ViewportToWorldPoint (new Vector3 (1, 1, distance));
        // scale the gameobject so it touches the cameras view frustrum
        Vector3 scale = viewTopRight - viewBottomLeft;
        scale.z = transform.localScale.z;
        transform.localScale = scale;
        //return the camera to it's original rotation
        _camera.transform.rotation = Quaternion.Euler (camRotation);

    }

    private float GetScale (int width, int height, Vector2 scalerReferenceResolution, float scalerMatchWidthOrHeight)
    {
        return Mathf.Pow (width / scalerReferenceResolution.x, 1f - scalerMatchWidthOrHeight) *
            Mathf.Pow (height / scalerReferenceResolution.y, scalerMatchWidthOrHeight);
    }

    private float GetScale (int width, int height, CanvasScaler canvasScaler)
    {
        var scalerReferenceResolution = canvasScaler.referenceResolution;
        var widthScale = width / scalerReferenceResolution.x;
        var heightScale = height / scalerReferenceResolution.y;

        switch (canvasScaler.screenMatchMode)
        {
            case CanvasScaler.ScreenMatchMode.MatchWidthOrHeight:
                var matchWidthOrHeight = canvasScaler.matchWidthOrHeight;

                return Mathf.Pow (widthScale, 1f - matchWidthOrHeight) *
                    Mathf.Pow (heightScale, matchWidthOrHeight);

            case CanvasScaler.ScreenMatchMode.Expand:
                return Mathf.Min (widthScale, heightScale);

            case CanvasScaler.ScreenMatchMode.Shrink:
                return Mathf.Max (widthScale, heightScale);

            default:
                throw new ArgumentOutOfRangeException ();
        }
    }

    //ScreenResolution/2-taille*0.5GetScale
    private void Update ()
    {
        if (Input.touchCount > 0) // if we have finger on screen
        {
            // get the first touch
            Touch first = Input.GetTouch (0);

            if (first.phase == TouchPhase.Began)
            {
                start = first.position;
            }
            else if (first.phase == TouchPhase.Moved)
            {
                Vector2 NewPosition = (first.position - start);
                start = first.position;

                rt.anchoredPosition += NewPosition;
            }
        }
    }

}