﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// [ExecuteInEditMode]
[ExecuteAlways]
[RequireComponent (typeof (GridLayoutGroup))]
public class AdjustCellSize : MonoBehaviour
{
    private RectTransform rect_transform;
    private GridLayoutGroup grid_layout;
    private int child_transform_count;

    void OnGUI ()
    {
        grid_layout = this.GetComponent<GridLayoutGroup> ();
        rect_transform = this.GetComponent<RectTransform> ();
        child_transform_count = this.transform.childCount;

        float width = rect_transform.rect.width;
        float height = rect_transform.rect.height;

        if (child_transform_count != 0)
        {
            if (grid_layout.constraint == GridLayoutGroup.Constraint.FixedColumnCount)
            {
                float width_per_cell = width / grid_layout.constraintCount - (grid_layout.spacing.x + grid_layout.padding.left);
                float height_per_cell = height / (child_transform_count / grid_layout.constraintCount) - (grid_layout.spacing.y + grid_layout.padding.top);
                grid_layout.cellSize = new Vector2 (width_per_cell, height_per_cell);
            }
            else if (grid_layout.constraint == GridLayoutGroup.Constraint.FixedRowCount)
            {
                float width_per_cell = width / (child_transform_count / grid_layout.constraintCount) - (grid_layout.spacing.x + grid_layout.padding.left);
                float height_per_cell = height / grid_layout.constraintCount - (grid_layout.spacing.y + grid_layout.padding.top);
                grid_layout.cellSize = new Vector2 (width_per_cell, height_per_cell);
            }
        }

        // TODO: finish this
        // else if (grid_layout.constraint == GridLayoutGroup.Constraint.Flexible)
        // {   
        //     float laukums = width * height;
        //     float laukums2 = width / height;
        //     float iespejamie = laukums / (child_transform_count);
        //     Debug.Log(iespejamie);

        //     int addCell = 0;
        //     // float width_per_cell = 0;
        //     // float height_per_cell = 0;
        //     if ((child_transform_count % 2) != 0 && (child_transform_count - 1) > 2)
        //     {
        //         child_transform_count = (child_transform_count - 1 )/2;
        //         addCell ++;

        //     }
        //     else if ( (child_transform_count % 2) == 0 ) {
        //         child_transform_count = this.transform.childCount / 2;
        //         addCell = 0;
        //     }

        //     if (width > height)
        //     {
        //         Debug.Log("width");
        //         float width_per_cell = width / child_transform_count - (grid_layout.spacing.x + grid_layout.padding.left);
        //         float height_per_cell = height / child_transform_count  - (grid_layout.spacing.y + grid_layout.padding.top);
        //         grid_layout.cellSize = new Vector2 (width_per_cell, height_per_cell);
        //     }
        //     else
        //     {
        //         Debug.Log("height");
        //     }
        //     // float width_per_cell = width / (child_transform_count / grid_layout.constraintCount) - (grid_layout.spacing.x + grid_layout.padding.left);
        //     // float height_per_cell = height / grid_layout.constraintCount - (grid_layout.spacing.y + grid_layout.padding.top);
        //     // grid_layout.cellSize = new Vector2 (width_per_cell, height_per_cell);

        //     // grid_layout.cellSize = new Vector2 (width_per_cell, height_per_cell);
        // }

    }
}