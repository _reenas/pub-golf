﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// [ExecuteInEditMode]
[RequireComponent (typeof (Camera))]
public class ScaleCamera : MonoBehaviour
{

    public Camera _camera;
    public Vector2 targetAspect = new Vector2 (9, 16);

    public float horizontalFoV = 60.0f;
    public float verticalFoV = 60.0f;

    void Start ()
    {
        _landscapeModeOnly = landscapeModeOnly;
        cam = _camera;
        if (!cam)
        {
            cam = Camera.main;
            Debug.Log ("Setting the main camera " + cam.name);
        }
        else
        {
            Debug.Log ("Setting the main camera " + cam.name);
        }

        if (!cam)
        {
            Debug.LogError ("No camera available");
            return;
        }
        wantedAspectRatio = _wantedAspectRatio;
        // UpdateCrop2 ();
        SetCamera ();
    }

    void Update ()
    {

    }

    // Call this method if your window size or target aspect change.
    public void UpdateCrop ()
    {
        // This is for horizontal layout
        // Determine ratios of screen/window & target, respectively.
        float screenRatio = Screen.width / (float) Screen.height;
        Debug.Log (screenRatio);
        float targetRatio = targetAspect.x / targetAspect.y;

        if (Mathf.Approximately (screenRatio, targetRatio))
        {
            // Screen or window is the target aspect ratio: use the whole area.
            _camera.rect = new Rect (0, 0, 1, 1);
        }
        else if (screenRatio > targetRatio)
        {
            // Screen or window is wider than the target: pillarbox.
            float normalizedWidth = targetRatio / screenRatio;
            float barThickness = (1f - normalizedWidth) / 2f;
            _camera.rect = new Rect (barThickness, 0, normalizedWidth, 1);
        }
        else
        {
            // Screen or window is narrower than the target: letterbox.
            float normalizedHeight = screenRatio / targetRatio;
            float barThickness = (1f - normalizedHeight) / 2f;
            _camera.rect = new Rect (0, barThickness, 1, normalizedHeight);
        }
    }

    public void UpdateCrop2 ()
    {
        float screenRatio = Screen.height / (float) Screen.width;
        Debug.Log (screenRatio);
        float targetRatio = targetAspect.x / targetAspect.y;
        Debug.Log (targetRatio);

        if (Mathf.Approximately (screenRatio, targetRatio))
        {
            // Screen or window is the target aspect ratio: use the whole area.
            _camera.rect = new Rect (0, 0, 1, 1);
        }
        else if (screenRatio > targetRatio)
        {
            // Screen or window is wider than the target: pillarbox.
            float normalizedWidth = targetRatio / screenRatio;
            float barThickness = (1f - normalizedWidth) / 2f;
            _camera.rect = new Rect (barThickness, 0, normalizedWidth, 1);
        }
        else
        {
            // Screen or window is narrower than the target: letterbox.
            float normalizedHeight = screenRatio / targetRatio;
            float barThickness = (1f - normalizedHeight) / 2f;
            _camera.rect = new Rect (0, barThickness, 1, normalizedHeight);
        }

    }

    public void UpdateFoV ()
    {
        // if(vert) {
        //     float halfHeight = 
        // }

        float halfWidth = Mathf.Tan (0.5f * horizontalFoV * Mathf.Deg2Rad);

        float halfHeight = halfWidth * Screen.height / Screen.width;

        float verticalFoV = 1.0f * Mathf.Atan (halfHeight) * Mathf.Rad2Deg;

        _camera.fieldOfView = verticalFoV;
    }

    public float _wantedAspectRatio = 1.5f;
    public bool landscapeModeOnly = true;
    static public bool _landscapeModeOnly = false;
    static float wantedAspectRatio;
    static Camera cam;
    static Camera backgroundCam;

    public static void SetCamera ()
    {
        float currentAspectRatio = 0.0f;
        if (Screen.orientation == ScreenOrientation.LandscapeRight ||
            Screen.orientation == ScreenOrientation.LandscapeLeft)
        {
            Debug.Log ("Landscape detected...");
            currentAspectRatio = (float) Screen.width / Screen.height;
        }
        else
        {
            Debug.Log ("Portrait detected...?");
            if (Screen.height > Screen.width && _landscapeModeOnly)
            {
                currentAspectRatio = (float) Screen.height / Screen.width;
            }
            else
            {
                currentAspectRatio = (float) Screen.width / Screen.height;
            }
        }
        // If the current aspect ratio is already approximately equal to the desired aspect ratio,
        // use a full-screen Rect (in case it was set to something else previously)

        Debug.Log ("currentAspectRatio = " + currentAspectRatio + ", wantedAspectRatio = " + wantedAspectRatio);

        if ((int) (currentAspectRatio * 100) / 100.0f == (int) (wantedAspectRatio * 100) / 100.0f)
        {
            cam.rect = new Rect (0.0f, 0.0f, 1.0f, 1.0f);
            if (backgroundCam)
            {
                Destroy (backgroundCam.gameObject);
            }
            return;
        }

        // Pillarbox
        if (currentAspectRatio > wantedAspectRatio)
        {
            float inset = 1.0f - wantedAspectRatio / currentAspectRatio;
            cam.rect = new Rect (inset / 2, 0.0f, 1.0f - inset, 1.0f);
        }
        // Letterbox
        else
        {
            float inset = 1.0f - currentAspectRatio / wantedAspectRatio;
            cam.rect = new Rect (0.0f, inset / 2, 1.0f, 1.0f - inset);
        }
        if (!backgroundCam)
        {
            // Make a new camera behind the normal camera which displays black; otherwise the unused space is undefined
            backgroundCam = new GameObject ("BackgroundCam", typeof (Camera)).GetComponent<Camera> ();
            backgroundCam.depth = int.MinValue;
            backgroundCam.clearFlags = CameraClearFlags.SolidColor;
            backgroundCam.backgroundColor = Color.black;
            backgroundCam.cullingMask = 0;
        }
    }

}

/*

    // Set this to the in-world distance between the left & right edges of your scene.
    public float sceneWidth = 10;

    Camera _camera;
    void Start() {
        _camera = GetComponent<Camera>();
    }

    // Adjust the camera's height so the desired scene width fits in view
    // even if the screen/window size changes dynamically.
    void Update() {
        float unitsPerPixel = sceneWidth / Screen.width;

        float desiredHalfHeight = 0.5f * unitsPerPixel * Screen.height;

        camera.orthographicSize = desiredHalfHeight;
    }

 */