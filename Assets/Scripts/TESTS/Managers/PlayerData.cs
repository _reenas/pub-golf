﻿#region Includes
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEditor;
using UnityEngine;
#endregion

#region Player Data
[System.Serializable]
public class PlayerData
{
    public int id;
    public string playerName;

    // TESTS
    public List<LevelSaveData> levels = new List<LevelSaveData>(); // Cant serialize
    public Dictionary<WorldSaveData, List<LevelSaveData>> doneDictionary =
        new Dictionary<WorldSaveData, List<LevelSaveData>>();
    // public OrderedDictionary orderedDone = new OrderedDictionary();

    public List<WorldSaveData> worldWhitelist = new List<WorldSaveData>();

    public PlayerData()
    {
        id = 0;
        playerName = " ";
        if (worldWhitelist.Count < 0)
        {
            Debug.Log(worldWhitelist.Count);
            WorldData wd = new WorldData();
            worldWhitelist.Add(new WorldSaveData(wd));
        }
    }
}

[System.Serializable]
public class LevelSaveData
{
    public string name = "";
    public int currentStars = 0;
    public bool isUnlocked = false;

    public LevelSaveData(LevelData config)
    {
        name = config.name;
        currentStars = config.currentStars;
        isUnlocked = config.isUnlocked;
    }
}

[System.Serializable]
public class WorldSaveData
{
    public int id = 0;
    public string name;
    public List<LevelSaveData> levels = new List<LevelSaveData>();
    public bool isUnlocked = false;

    public WorldSaveData(WorldData data)
    {
        id = data.worldId;
        name = data.wordName;
        isUnlocked = data.isUnlocked;
    }
}

#endregion

#region Game Data

[System.Serializable]
public class GameData
{
    public static GameData instance;
    public static int s_Version { get; private set; }
    private PlayerData playerData = new PlayerData(); // Maybe inherit for bit more protection?

    // Game settings
    public float masterVolume = float.MinValue;
    public float musicVolume = float.MinValue;
    public float masterSFXVolume = float.MinValue;

    public int qualityLevel = 0;

    // Specific vars
    public static string passedTime { get; private set; }

    public GameData()
    {
        instance = this;
        Debug.Log("Version : " + s_Version);

    }

    public static void Create()
    {
        instance = new GameData();

        // First interaction
        if (s_Version == 0)
        {
            // Player setup
            instance.playerData = new PlayerData();
            instance.playerData.id = 0; // Get from server
            instance.playerData.playerName = "User: " + instance.playerData.id.ToString();

            // Quality settings
            instance.qualityLevel = 2;

            // Audio settings
            // ..

            // If everythiing done increase version;
            s_Version++;
            SaveData.Save();
        }
        else if (s_Version == 1)
        {
            // Some changes ... TODO:
            // i.e. game sets first levels for tutorial purposes before showing real levels
            SaveData.Load();
        }
    }

    private void ClearData()
    {

    }

    public void AddLevel(WorldSaveData _world, LevelSaveData _level)
    {
        if (ContainsWorld(_world))
        {
            // playerData.doneDictionary[_world].Add(_level);
            UnlockLevel(_level);
            GetWorld(_world).levels.Add(_level);
        }
        else
        {
            playerData.worldWhitelist.Add(_world);
            GetWorld(_world).levels.Add(_level);
            // playerData.doneDictionary.Add(_world, new List<LevelSaveData>());
            // playerData.doneDictionary[_world].Add(_level);
        }
    }

    public bool ContainsWorld(WorldSaveData _world)
    {
        bool contains = false;
        foreach (WorldSaveData savedWorld in playerData.worldWhitelist)
        {
            if (savedWorld.name == _world.name)
            {
                contains = true;
                break;
            }
        }
        return contains;
        // return playerData.doneDictionary.ContainsKey(_key);
    }

    public WorldSaveData GetWorld(WorldSaveData _world)
    {
        WorldSaveData world = null;

        foreach (WorldSaveData savedWorld in playerData.worldWhitelist)
        {
            if (savedWorld.name == _world.name)
            {
                world = savedWorld;
                break;
            }
            else
            {
                world = null;
            }
        }

        return world;
    }

    public WorldSaveData GetLastWorld()
    {
        return playerData.worldWhitelist[playerData.doneDictionary.Count];
    }

    public int GetWorldCount()
    {
        int temp = 0;
        if (playerData.doneDictionary.Count > 0)
            temp = playerData.doneDictionary.Count;
        return temp;
    }

    public LevelSaveData GetLastLevel()
    {
        return GetLastWorld().levels[GetLastWorld().levels.Count];
    }

    public int GetLevelCount()
    {
        int temp = 0;
        if (playerData.worldWhitelist[GetWorldCount()].levels.Count > 0)
            temp = playerData.worldWhitelist[GetWorldCount()].levels.Count;
        return temp;
    }

    public void UnlockWorld(WorldSaveData _world, bool _unlock = true)
    {
        _world.isUnlocked = _unlock;
    }

    public void UnlockLevel(LevelSaveData _level, bool _unlock = true)
    {
        _level.isUnlocked = _unlock;
    }

    public void ChangeLevelStars(LevelSaveData _level, int _starCount = 0)
    {
        _level.currentStars = _starCount;
    }
}

#endregion

/* 

TODO: 
    - Make save and loding generic
    - Or make game data more modular
    - 
*/
public static class SaveData
{
    // public static List<Game> savedGames = new List<Game>(); // If we have many saves use this
    public static GameData saveGame = new GameData();

    // To easy set and create save game names
    private static string saveGameName = "/pubgolf.lum";

    public static void Save()
    {
        // savedGames.Add(Game.current); // If we have many saves use this
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/" + saveGameName);
        bf.Serialize(file, saveGame);
        file.Close();
    }

    // public static void Save()
    // {
    //     // savedGames.Add(Game.current); // If we have many saves use this
    //     BinaryFormatter bf = new BinaryFormatter();
    //     FileStream file = File.Create(Application.persistentDataPath + "/pubgolf.lum");
    //     bf.Serialize(file, saveGame);
    //     file.Close();
    // }

    public static void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/" + saveGameName))
        {
            Debug.Log("Loading save game");
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/" + saveGameName, FileMode.Open);
            // savedGames = (List<Game>)bf.Deserialize(file); // If we have many saves use this
            saveGame = (GameData)bf.Deserialize(file);
            file.Close();
        }
        else
        {
            Debug.Log("Created new save game");
            Save();
        }
    }
}


public static class Converter
{
    public static void WorldToSave(WorldData _world) {

    }

    public static LevelData SaveToLevel(LevelSaveData _level) {
        LevelData ld = new LevelData();
        // ld.id = _level.id;
        ld.name = _level.name;
        ld.currentStars = _level.currentStars;
        ld.isUnlocked = _level.isUnlocked;

        return ld;
    }
}




// Test foo = new Test {
//     Id = 0,
//     FirstName = "a"
// };

// public class Test
// {
//     public int Id;
//     public string FirstName;
// }