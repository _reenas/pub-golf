﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    static public GameManager instance { get { return p_Instance; } }
    static protected GameManager p_Instance;

    [Header("Game States")]
    public State[] a_States;
    public StateManager m_StateManager = new StateManager();
    public string c_state = "";

    public float sqrShakeTreshold;
    public float shakeInterval;

    private float lastShakeTime;
    public bool shakedPhone = false;

    public Joystick joystick;
    public Camera playerCam;
    public GameObject gameTarget;

    private void OnEnable()
    {
        p_Instance = this;
        Application.targetFrameRate = 60;

        if (a_States.Length <= 0)
        {
            Debug.LogWarning("No states");
        }
        else
        {
            m_StateManager.Create(a_States);
        }

        if (c_state != "")
        {
            m_StateManager.SwitchState(c_state);
        }
    }

    private void Start()
    {
        // Time.timeScale = 0.2f;
        sqrShakeTreshold = Mathf.Pow(sqrShakeTreshold, 2);
    }

    private void Update()
    {
        if (m_StateManager.stackCount > 0)
        {
            m_StateManager.stateStack[m_StateManager.stackCount - 1].Tick();
        }

        // TODO: Check this
        // Detect phone shake
        if (Input.acceleration.sqrMagnitude >= sqrShakeTreshold &&
            Time.unscaledTime >= lastShakeTime + shakeInterval)
        {
            shakedPhone = true;
            lastShakeTime = Time.unscaledTime;
        }
    }

    public void UpdateCameraTarget()
    {
        playerCam.GetComponent<CameraFollow>().target = gameTarget.transform;
    }

    // Later
    //     protected void OnApplicationQuit ()
    //     {
    // #if UNITY_ANALYTICS
    //         // We are exiting during game, so this make this invalid, send an event to log it
    //         // NOTE : this is only called on standalone build, as on mobile this function isn't called
    //         bool inGameExit = m_StateStack[m_StateStack.Count - 1].GetType () == typeof (GameState);

    //         Analytics.CustomEvent ("user_end_session", new Dictionary<string, object>
    //         { { "force_exit", inGameExit },
    //             { "timer", Time.realtimeSinceStartup }
    //         });
    // #endif
    //     }
}