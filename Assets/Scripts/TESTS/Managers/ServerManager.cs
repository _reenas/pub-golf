﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

using TMPro;
using UnityEngine.Networking;

[System.Serializable]
public class User_List
{
    public User[] users;
}

[System.Serializable]
public class User
{
    public string id;
    public string username;
    public int score;
    public string money;
}

public class ServerManager : MonoBehaviour
{

    public static ServerManager instance = null;
    public GameManager gameManager;
    
    // [TextArea]
    [SerializeField]
    private static string loaded_text = "";

    public List<User> Players = new List<User> ();

    // private bool start = false;

    public IEnumerator GetUsers (string _url, System.Action<string> _return_text)
    {
        string return_text = "";

        if (_url.Contains ("://") || _url.Contains (":///"))
        {
            UnityWebRequest request = UnityWebRequest.Get (_url);
            yield return request.SendWebRequest ();

            return_text = request.downloadHandler.text;

            if (request.isDone)
            {
                
            }
        }
        else
        {
            Debug.LogWarning ("File doesn't exist");
        }

        _return_text (return_text);
    }

    public IEnumerator Load ()
    {
        // Get all users
        yield return StartCoroutine (GetUsers ("http://lumiosgames.com/cubix/get_users.php", value => loaded_text = value));
        LoadUsers ();
    }

    public void LoadUsers ()
    {

        User_List user_list = JsonUtility.FromJson<User_List> (loaded_text);

        Players.Clear ();

        for (int i = 0; i < user_list.users.Length; i++)
        {
            Players.Add (user_list.users[i]);
        }

        // Sorts leaderbord by score
        Players.Sort ((a, b) => b.score.CompareTo (a.score));

        gameManager.enabled = true;

    }

    void Awake ()
    {
        if (instance == null)
        {
            instance = this;
        }

        StartCoroutine ("Load");
    }

    public void Refresh ()
    {
        StartCoroutine ("Load");
    }

    public IEnumerator Upload (string id, string username, string score, int money)
    {
        WWWForm form = new WWWForm ();
        form.AddField ("id", id);
        form.AddField ("username", username);
        form.AddField ("score", score);
        form.AddField ("money", money);

        using (UnityWebRequest www = UnityWebRequest.Post ("http://lumiosgames.com/cubix/save_user.php", form))
        {
            yield return www.SendWebRequest ();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log (www.error);
            }
            else
            {
                Debug.Log (www.downloadHandler.text);
            }
        }
    }

    public IEnumerator UpdateUsername (string id, string username)
    {
        WWWForm form = new WWWForm ();
        form.AddField ("id", id);
        form.AddField ("username", username);

        using (UnityWebRequest www = UnityWebRequest.Post ("http://lumiosgames.com/cubix/upload_username.php", form))
        {
            yield return www.SendWebRequest ();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log (www.error);
            }
            else
            {
                Debug.Log (www.downloadHandler.text);
            }
        }
    }
}