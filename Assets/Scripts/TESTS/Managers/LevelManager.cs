﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    static public LevelManager instance { get { return p_Instance; } }
    static protected LevelManager p_Instance;

    [Header("Worlds")]

    public static List<WorldData> s_worlds;
    [SerializeField] public List<WorldData> worlds;

    [Header("World/Level data")]
    public static int worldID;
    public static int levelID;

    [Header("Current Level Info")]
    [Tooltip("For loading specific level without any requirements")]
    public LevelData currentLevelData; // TODO: Add dropdown menu for avaible levels
    public static LevelData s_currentLevel;

    [ReadOnly] public string levelName;
    [ReadOnly] public string worldName;

    [Header("Player data")]
    public GameObject playerPrefab;
    public GameObject playerObject { get; private set; }
    private Vector3 playerPoint;

    private void Awake()
    {
        if (p_Instance == null) p_Instance = this;

        Create();
    }

    public void Create()
    {
        s_worlds = worlds;

        // Debug.Log(s_worlds.Count);
    }

    // Function for selecting custom level or last avaible
    public void SelectCurrentLevel()
    {
        // Selects specific level
        if (currentLevelData != null)
        {
            SetLevel(GetLevelIndex(currentLevelData));
        }
        else if (GameData.instance.GetLevelCount() > 0) // Select last possible level
        {
            bool exitInner = false;
            for (int _worldID = 0; _worldID < s_worlds.Count; _worldID++)
            {
                for (int _levelID = 0; _levelID < s_worlds[_worldID].levels.Count; _levelID++)
                {
                    if (s_worlds[_worldID].levels.Contains(Converter.SaveToLevel(GameData.instance.GetLastLevel())))
                    {
                        SetLevel(_worldID, _levelID);
                        exitInner = true;
                    }
                }
                if (exitInner)
                {
                    break;
                }
            }
        }
        else
        {
            SetLevel(0, 0);
        }
    }


    public void SetLevel(Vector2 _levelIndex)
    {
        worldID = (int)_levelIndex.x;
        levelID = (int)_levelIndex.y;
        s_currentLevel = s_worlds[worldID].levels[levelID];
        currentLevelData = s_currentLevel;
    }

    public void SetLevel(int _world, int _level)
    {
        worldID = _world;
        levelID = _level;
        s_currentLevel = s_worlds[worldID].levels[levelID];
        currentLevelData = s_currentLevel;
    }

    public void LoadLevel()
    {
        GameObject currentLevelPrefab = s_currentLevel.levelPrefab;
        GameObject currentLevelObject = Instantiate(currentLevelPrefab, transform, true);
        Vector3 playerStartPosition = currentLevelPrefab.GetComponent<Level>().levelData.startPoint.position;

        LoadPlayer(currentLevelObject.transform, playerStartPosition);
    }

    public void ReloadLevel()
    {
        UnloadLevel();
        LoadLevel();
    }

    public void UnloadLevel()
    {
        Destroy(this.transform.GetChild(worldID).gameObject);
        UnloadPlayer();
    }


    public Vector2 GetLevelIndex(LevelData _levelData)
    {
        Vector2 levelIndex = new Vector2();
        for (int i = 0; i < s_worlds.Count; i++)
        {
            if (s_worlds[i].levels.Contains(_levelData))
            {
                levelIndex.x = i;
                levelIndex.y = s_worlds[i].levels.IndexOf(_levelData);
                break;
            }
        }
        return levelIndex;
    }

    public bool NextLevel()
    {
        Vector2 levelIndex = GetLevelIndex(currentLevelData); // references current level index

        if (levelIndex.y + 1 < s_worlds[worldID].levels.Count) // if level list is not reached the last level 
        {
            levelIndex.y++; // Change level index to next
            s_worlds[worldID].levels[(int)levelIndex.y].isUnlocked = true;
            return true;
        }
        else if (levelIndex.x + 1 < s_worlds.Count) // Change to next world
        {
            levelIndex.x++; // Changes current world index
            s_worlds[worldID].isUnlocked = true; // Unlocks next world
            levelIndex.y = 0; // Resets level counter
            return true;
        }
        return false;
    }

    public void UpdateNames()
    {
        levelName = "World " + worldID + " Level " + levelID;
        worldName = worlds[worldID].name;
    }

    public void UnlockNextLevel(LevelData _levelData)
    {
        Vector2 levelIndex = GetLevelIndex(currentLevelData); // references current level index

        if (levelIndex.y + 1 < s_worlds[worldID].levels.Count) // if level list is not reached the last level 
        {
            levelIndex.y++; // Change level index to next
            s_worlds[worldID].levels[(int)levelIndex.y].isUnlocked = true;
        }
        else if (levelIndex.x + 1 < s_worlds.Count) // Change to next world
        {
            levelIndex.x++; // Changes current world index
            s_worlds[worldID].isUnlocked = true; // Unlocks next world
            levelIndex.y = 0; // Resets level counter
        }
    }

    // TODO: SHOULD BE IN GAME OVER
    public void CheckHighScore(int _current)
    {
        if (_current > currentLevelData.currentStars)
        {
            currentLevelData.currentStars = _current;
            s_currentLevel.currentStars = _current;
        }
    }

    #region PLAYER OBJECT LOADING AND UNLOADING

    private void LoadPlayer(Transform _parent, Vector3 _position)
    {
        playerObject = Instantiate(playerPrefab, _parent, true);
        playerObject.transform.position = _position;
        GameManager.instance.gameTarget = playerObject;
        GameManager.instance.UpdateCameraTarget();
    }

    private void UnloadPlayer()
    {
        if (playerObject != null)
        {
            Destroy(playerObject);
        }
    }
    #endregion

}



/* 
TODO: NAHUJ NEVAJAG WTF :@ nu mosk tomer?!
if (PlayerData.donelevels.Count == 0)
{
    for (int f = 0; f < 30; f++)
    {
        int zero = 0;
        PlayerData.donelevels.Add(zero);
    }
}

int world_id = 0;
int level_id = 0;
for (int d = 0; d < 30; d++)
{
    if (((d % 10) == 0 && d != 0) && (world_id + 1 <= worlds.Length))
    {
        world_id = world_id + 1;
        level_id = 0;
    }
    worlds[world_id].levels[level_id].currentStars = PlayerData.donelevels[d];
    level_id++;
}

for (int i = 0; i < worlds.Count; i++)
{
    for (int j = 0; j < worlds[i].levels.Length; j++)
    {
        SetStartEndPoint(worlds[i].levels[j]);
    }
}

*/
