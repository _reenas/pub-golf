﻿#define UNITY_IOS
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(LineRenderer))]
public class PlayerManager : MonoBehaviour
{
    // public GameObject playerObject;
    [Header("Components")]
    public Joystick jsController; // TODO: Remove
    public Camera playerCamera;
    public Rigidbody rigidbodyComponent;
    // public LineRenderer lineRenderer;
    public LineRenderer lineRenderer2;
    public GameObject collisionCircle;
    public Transform targetArrow;

    [Header("Audio Sources")]
    public AudioSource audioSource;
    public AudioSource audioSourceTwo;
    public AudioClip[] sounds;

    [Header("Bools")]
    public bool canMove = false;
    public bool setMoving = false;
    public bool hollding = false;
    public bool hitTable = false;
    public bool hitObstacle = false;
    public static bool onDrag = false;

    [Header("Other")]
    // Floats
    public float throwForce = 2f;
    public float endForce;
    public float lineWidth = 0f;
    public float moveMagnitude;
    public float playerRadius;
    private float hitDistance;

    // Vectors
    public Vector3 direction;
    public Vector3 lastPosition;
    private Vector3 playerVelocity;

    // Ints
    // public int moveCount = 0;
    public LayerMask layerMask = 8;

    // Other
    private RaycastHit hit = new RaycastHit();
    private Ray secondLine;

    Vector3 cameraFoward;
    Quaternion cameraRotation;

    #region Unity functions

    private void Awake()
    {

        // playerRadius = transform.GetChild (0).GetComponent<MeshCollider> ().bounds.size.x / 2;
        playerRadius = transform.GetComponent<MeshCollider>().bounds.size.x / 2;

        lineRenderer2.startWidth = lineWidth;
        lineRenderer2.endWidth = lineWidth;
        lineRenderer2.enabled = false;
        
        jsController = GameManager.instance.joystick;
        playerCamera = GameManager.instance.playerCam;
    }

    private void OnEnable()
    {
        rigidbodyComponent.velocity = Vector3.zero;
        transform.rotation = Quaternion.identity;
        collisionCircle.SetActive(false);
        targetArrow.gameObject.SetActive(false);
        VARS.moveCount = 0;
    }

    void Start() { }


    // Update is called once per frame
    void Update()
    {
        // if (playerCamera.GetComponent<CameraFollow>().canRotate)
        // {
        //     cameraFoward = playerCamera.transform.up;
        //     cameraFoward.y = 0;
        //     cameraRotation = Quaternion.FromToRotation(Vector3.forward, cameraFoward);
        // }

        if (GameManager.instance.shakedPhone)
        {
            ShakePlayer();
            GameManager.instance.shakedPhone = false;
        }

        if (hitTable)
        {
            hitTable = false;
            audioSourceTwo.clip = sounds[1];
            audioSourceTwo.Play();
        }

        if (hitObstacle)
        {
            hitObstacle = false;
            audioSourceTwo.clip = sounds[2];
            audioSourceTwo.Play();
        }

        if (rigidbodyComponent.velocity.magnitude <= 0.001)
        {
            audioSource.Stop();
        }

        if (onDrag && setMoving && !GameManager.instance.shakedPhone)
        {
            hollding = true;
            direction = (cameraRotation * jsController.Direction) * -1;

            moveMagnitude = jsController.Direction.magnitude;
            moveMagnitude = Mathf.Round(moveMagnitude * 20f) / 20f;

            direction.Normalize();
            float directionRounder = 40f;
            direction.x = Mathf.Round(direction.x * directionRounder) / directionRounder;
            direction.y = Mathf.Round(direction.y * directionRounder) / directionRounder;
            direction.z = Mathf.Round(direction.z * directionRounder) / directionRounder;

            endForce = moveMagnitude;

            Ray playerRay = new Ray(GetPlayerPosition(), direction);
            // Ray secondLine;

            if (moveMagnitude >= .2f)
            {
                canMove = true;

                if (Deflect(playerRay, out secondLine, out hit))
                {
                    DrawTwoLines(playerRay.origin, GetPlayerPosition() + ((direction / 2) * endForce),
                        playerRay.origin + direction * hitDistance, secondLine.origin + secondLine.direction / 6);
                }
                else
                {
                    DrawArrow(playerRay.origin, GetPlayerPosition() + (direction / 2 * endForce));
                    collisionCircle.SetActive(false);
                }
            }
            else
            {
                targetArrow.gameObject.SetActive(false);
                collisionCircle.SetActive(false);
                canMove = false;
            }
        }

        if (rigidbodyComponent.velocity.magnitude > 0)
        {
            setMoving = false;
        }
        else if (Mathf.Approximately(rigidbodyComponent.velocity.magnitude, 0))
        {
            setMoving = true;
        }

        // if (canMove && !onDrag && Mathf.Approximately (rigidbodyComponent.velocity.magnitude, 0))
        if (canMove && !onDrag && setMoving)
        {
            // hollding = false;
            audioSource.clip = sounds[0];
            audioSource.Play();

            endForce = (moveMagnitude - 0.1f) * throwForce;
            // endForce = Mathf.Round (endForce * 2f) / 2f;

            // Maybe round only this, but then again, we have diferent decimal places
            // playerVelocity = direction * endForce;
            rigidbodyComponent.AddForce(direction * endForce, ForceMode.Force);

            // rigidbodyComponent.velocity = playerVelocity;

            collisionCircle.SetActive(false);
            targetArrow.gameObject.SetActive(false);
            lineRenderer2.enabled = false;

            VARS.moveCount++;
            canMove = false;
        }

        if (!onDrag)
        {
            hollding = false;
        }

        lastPosition = transform.position;
        // END OF UPDATE
    }

    #endregion // Unity functions

    Vector3 GetPlayerPosition() { return transform.position; }

    Vector3 GetPlayerEndPosition(Vector3 direction)
    {
        Vector3 endPostion = Vector3.zero;
        endPostion = GetPlayerPosition() + direction;
        return endPostion;
    }

    public void DrawArrow(Vector3 start, Vector3 end)
    {
        targetArrow.gameObject.SetActive(true);
        // Draw arrow color for hit strengt
        Color arrowColor = targetArrow.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color;

        arrowColor = new Color(2.0f * endForce, 2.0f * (1 - endForce), 0f);
        targetArrow.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = arrowColor;

        // adjust arrow height
        end.y = transform.position.y + 0.13f;
        targetArrow.position = end;
        float angle = Mathf.Atan2(direction.z, direction.x) * Mathf.Rad2Deg;
        targetArrow.rotation = Quaternion.AngleAxis(-angle, Vector3.up);
    }

    void DrawTwoLines(Vector3 start, Vector3 firstEnd, Vector3 cross, Vector3 end)
    {
        DrawArrow(start, firstEnd);
        if (!collisionCircle.activeSelf)
        {
            collisionCircle.transform.position = new Vector3(cross.x, transform.position.y + 0.01f, cross.z);
        }
        collisionCircle.SetActive(true);

        cross.y = transform.position.y + 0.01f;
        end.y = transform.position.y + 0.01f;
        collisionCircle.transform.position = Vector3.Lerp(collisionCircle.transform.position, new Vector3(cross.x, transform.position.y + 0.01f, cross.z), 10f * Time.deltaTime);

        if (!lineRenderer2.enabled)
        {
            lineRenderer2.SetPosition(0, cross);
            lineRenderer2.SetPosition(1, end);
        }
        lineRenderer2.enabled = true;

        lineRenderer2.SetPosition(0, Vector3.Lerp(lineRenderer2.GetPosition(0), cross, 10f * Time.deltaTime));
        lineRenderer2.SetPosition(1, Vector3.Lerp(lineRenderer2.GetPosition(1), end, 10f * Time.deltaTime));

    }

    bool Deflect(Ray ray, out Ray deflected, out RaycastHit hit)
    {
        if (Physics.SphereCast(ray, playerRadius, out hit, 100f, layerMask))
        {
            Vector3 normal = hit.normal;
            Vector3 deflect = Vector3.Reflect(ray.direction, normal);

            // rayOrigin = ray.origin;
            hitDistance = hit.distance;

            deflected = new Ray(hit.point, deflect);
            if (hit.transform.CompareTag("Card"))
            {
                deflected = new Ray(Vector3.zero, Vector3.zero);
                return false;
            }
            return true;
        }
        deflected = new Ray(Vector3.zero, Vector3.zero);
        return false;
    }

    public Vector3 GetWorldPoint(Vector3 mouse_screen_pos)
    {
        mouse_screen_pos.z = playerCamera.transform.position.y - transform.position.y;
        return playerCamera.ScreenToWorldPoint(mouse_screen_pos);
    }

    public void SetDefault()
    {
        VARS.moveCount = 0;
        rigidbodyComponent.velocity = new Vector3(0f, 0f, 0f);
        rigidbodyComponent.angularVelocity = new Vector3(0f, 0f, 0f);
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        direction = Vector3.zero;
        endForce = 0;
        collisionCircle.SetActive(false);
        targetArrow.gameObject.SetActive(false);
    }

    // If Player lands on mesh side player can shake phone to get rotation back to face
    void ShakePlayer()
    {
        rigidbodyComponent.AddForce(transform.up / 1, ForceMode.Impulse);
    }

    // Pans volume for sliding sound effect
    private void SetSlideVolume(float _volume)
    {
        audioSource.volume = _volume;
    }

    #region Collisions

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("End"))
        {
            // Debug.Log ("Entered circle");
        }
        if (other.CompareTag("TableBase"))
        {
            // Debug.Log ("Hit table base");
            hitTable = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("End")) // Debug.Log ("Exited circle");

            if (other.CompareTag("TableBase"))
            {
                // Debug.Log ("Exited table base");
                audioSource.Stop();
            }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("End"))
        {
            if (rigidbodyComponent.velocity == Vector3.zero)
            {
                // Debug.Log ("Circle Center");
                // TODO: create call back to level manager were its sends message to game scene
                // TODO: Add pause for win animation
                GameManager.instance.m_StateManager.SwitchState("GameOver");
            }
        }
    }

    private void OnCollisionStay(Collision other)
    {
        if (other.transform.CompareTag("Hit Floor"))
        {
            LevelManager.instance.ReloadLevel();
        }
        // if(other.transform.tag == "Obstacle") Debug.Log("collision");
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Obstacle"))
        {
            // Debug.Log ("Hit Obstacle");
            hitObstacle = true;
        }
    }
    #endregion // Collisions
}