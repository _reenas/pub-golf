﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VARS
{
    [System.Serializable]
    public struct STARS
    {
        public int ONE;
        public int TWO;
        public int THREE;
    }

    public static int moveCount;
}
