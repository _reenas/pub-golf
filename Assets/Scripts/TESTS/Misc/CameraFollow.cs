﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
/* TODO: 
	-Change y position target to table aka base platform
	- Add restart function for mainCamera reseting to start point
*/
public class CameraFollow : MonoBehaviour
{
    public Camera mainCamera;
    public Transform target;
    // public GameObject boundTarget;

    public float offset;
    public float dirMaginitude;
    private float defaultYPos = 0f;
    public float smoothSpeed = 0.125f;
    private float zoomOutMax = 3f;
    private float zoomOutMin = 1f;

    public float cameraMoveSpeed = 0;

    private Vector3 mainCameraPosition;
    private Vector3 direction;

    public Vector3 startPos;
    public Vector3 endPos;

    // Get all this from corespondig scripts
    public bool canFollow = false;
    public bool canRotate = true;
    // public bool cannotMove = false;

    private Vector2 initTouch;

    public Vector3 originalRotation;
    float rotationX = 0f;
    float rotationY = 0f;
    float cameraRotation = 0f;
    float rotationSpeed = 0.8f;

    void OnEnable()
    {

        mainCamera.transform.eulerAngles = originalRotation = new Vector3(90, 0, 0);
        // rotationX = originalRotation.z;
        rotationX = 0;
        rotationY = 0;

        // TODO: this should be done with cinemacine or tweening
        // startPos = LevelManager.worlds[LevelManager.worldID].levels[LevelManager.levelID].endPoint.transform.position;
        // endPos = LevelManager.worlds[LevelManager.worldID].levels[LevelManager.levelID].startPoint.transform.position;
        // endPos.y = endPos.y + offset;

        canFollow = false;

        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            defaultYPos = target.position.y + offset;
            target.GetComponent<PlayerManager>().canMove = false;
        }
		
		target = GameManager.instance.gameTarget.transform;
    }

    private void FixedUpdate()
    {
        if (Input.touchCount == 1)
        {
            Touch touch = Input.touches[0];
            if (touch.phase == TouchPhase.Began)
            {
                initTouch = touch.position;
            }
            else if (touch.phase == TouchPhase.Moved && !PlayerManager.onDrag)
            {
                canRotate = true;

                if (initTouch.x > touch.position.x)
                {
                    cameraRotation -= (initTouch.x - touch.position.x) * Time.deltaTime * rotationSpeed;
                }
                else if (initTouch.x < touch.position.x)
                {
                    cameraRotation += (initTouch.x - touch.position.x) * Time.deltaTime * rotationSpeed;
                }
                else if (initTouch.y > touch.position.y)
                {
                    cameraRotation -= (initTouch.y - touch.position.y) * Time.deltaTime * rotationSpeed;
                }
                else if (initTouch.y < touch.position.y)
                {
                    cameraRotation += (initTouch.y - touch.position.y) * Time.deltaTime * rotationSpeed;
                }
                // rotationX -= (initTouch.x - touch.position.x) * Time.deltaTime * rotationSpeed;
                // rotationY -= (initTouch.y - touch.position.y) * Time.deltaTime * rotationSpeed;
                // float rotation = (rotationY > rotationX) ? rotationY : rotationX;
                float newRotation = Mathf.Lerp(originalRotation.z, cameraRotation, 30f * Time.deltaTime);

                mainCamera.transform.eulerAngles = new Vector3(originalRotation.x, originalRotation.y, newRotation);
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                initTouch = Vector2.zero;
                canRotate = false;
            }
        }

        if (Input.touchCount == 2)
        {
            Vector2 touchZeroPrevPos = Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition;
            Vector2 touchOnePrevPos = Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition;

            float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float currentMagnitude = (Input.GetTouch(0).position - Input.GetTouch(1).position).magnitude;

            float difference = prevMagnitude - currentMagnitude;

            zoom(difference);
        }
    }

    private void Update()
    {
#if UNITY_EDITOR
        zoom(Input.GetAxis("Mouse ScrollWheel") * 100);
#endif
        if (!canFollow)
        {
            canFollow = true;
            transform.position = Vector3.Lerp(startPos, endPos, 0.001f * Time.deltaTime);
        }
        else
        {
            if (target != null)
            {
                if (target.GetComponent<PlayerManager>().hollding)
                {
                    direction = target.GetComponent<PlayerManager>().direction;
                    dirMaginitude = target.GetComponent<PlayerManager>().moveMagnitude;
                    if (dirMaginitude > 0.2f)
                    {
                        Vector3 smoothedPosition = Vector3.Lerp(transform.position, target.position + (direction.normalized / 2), smoothSpeed);
                        transform.position = smoothedPosition;
                    }
                }
                else
                {
                    Vector3 smoothedPosition = Vector3.Lerp(transform.position, target.position, smoothSpeed);
                    transform.position = smoothedPosition;
                }
            }
        }

        transform.position = new Vector3(transform.position.x, defaultYPos, transform.position.z);
    }

    // Only for orthograpic camera
    void zoom(float increment)
    {
        float camera_height = defaultYPos;
        camera_height = camera_height + increment;

        camera_height = Mathf.Lerp(defaultYPos, camera_height, 0.2f * Time.deltaTime);
        camera_height = Mathf.Clamp(camera_height, zoomOutMin, zoomOutMax);
        defaultYPos = camera_height;
    }

    public void OnRestart()
    {
        transform.position = target.position;
        mainCameraPosition = target.position;
    }
}