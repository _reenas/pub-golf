﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartLoaderAsyc : MonoBehaviour
{
    public static StartLoaderAsyc instance;
    public GameObject startCanvas;
    public GameObject startButton;

    public Slider slider;
    public TextMeshProUGUI loadText;

    IEnumerator LoadAsync (string scene_name)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync (scene_name);
        float progress = 0f;
        while (!operation.isDone)
        {
            progress = Mathf.Clamp01 (operation.progress / 0.9f);
            slider.value = progress;

            yield return null;
        }

        startCanvas.SetActive(false);
    }

    // Start is called before the first frame update
    void Start ()
    {
        
        Application.targetFrameRate = 60;
        // DontDestroyOnLoad (this);
        // instance = this;
    }

    public void Load() {
        startButton.SetActive(false);
        slider.gameObject.SetActive(true);
        loadText.gameObject.SetActive(true);
        StartCoroutine (LoadAsync ("MAIN"));
    }
}