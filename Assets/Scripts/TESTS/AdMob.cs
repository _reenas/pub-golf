﻿// using System;
// using System.Collections;
// using System.Collections.Generic;
// using GoogleMobileAds.Api;
// using UnityEngine;

// /*
//     TODO: Change app id's for ios
//  */

// public class AdMob : MonoBehaviour
// {
//     public static AdMob instance;
//     private BannerView bannerView;
//     private InterstitialAd interstitial;

//     public bool test = false;

//     void Start ()
//     {
// #if UNITY_ANDROID
//         string appId = "ca-app-pub-1803059063929026~9658894254";
// #elif UNITY_IPHONE
//         string appId = "ca-app-pub-1803059063929026~7728533781";
// #endif
//         if (instance == null)
//         {
//             instance = this;
//         }
//         // Initialize the Google Mobile Ads SDK.
//         MobileAds.Initialize (appId);

//         RequestBanner ();
//         RequestInterstitial ();
//         Debug.Log ("End of start");
//     }

//     private void RequestBanner ()
//     {
// #if UNITY_ANDROID
//         // Test code
//         // string adUnitId = "ca-app-pub-3940256099942544/6300978111";
//         // Real code
//         string adUnitId = "ca-app-pub-1803059063929026/6841159224";
// #elif UNITY_IPHONE
//         string adUnitId = "ca-app-pub-1803059063929026/1149373489";
// #endif

//         if (this.bannerView != null)
//         {
//             this.bannerView.Destroy ();
//         }
//         // Create a 320x50 banner at the top of the screen.
//         this.bannerView = new BannerView (adUnitId, AdSize.Banner, AdPosition.Bottom);

//         // Create an empty ad request.
//         AdRequest request = new AdRequest.Builder ().Build ();

//         this.bannerView.OnAdLoaded += HandleAdLoaded;
//         // Load the banner with the request.
//         this.bannerView.LoadAd (request);

//     }

//     private void RequestInterstitial ()
//     {
// #if UNITY_ANDROID
//         // Test code
//         // string adUnitId = "ca-app-pub-3940256099942544/1033173712";
//         // Real code
//         string adUnitId = "ca-app-pub-1803059063929026/7527570842";
// #elif UNITY_IPHONE
//         string adUnitId = "ca-app-pub-1803059063929026/2294934195";
// #else
//         string adUnitId = "unexpected_platform";
// #endif

//         // Initialize an InterstitialAd.
//         this.interstitial = new InterstitialAd (adUnitId);

//         // Create an empty ad request.
//         AdRequest request = new AdRequest.Builder ().Build ();

//         // Load the interstitial with the request.
//         this.interstitial.LoadAd (request);
//     }

//     public void ShowAdd ()
//     {
//         if (interstitial.IsLoaded ())
//         {
//             interstitial.Show ();
//         }
//         else
//         {
//             MonoBehaviour.print("Interstitial is not ready yet");
//         }
//     }
//     #region Banner callback handlers

//     public void HandleAdLoaded (object sender, EventArgs args)
//     {
//         MonoBehaviour.print ("HandleAdLoaded event received");
//     }

//     public void HandleAdFailedToLoad (object sender, AdFailedToLoadEventArgs args)
//     {
//         MonoBehaviour.print ("HandleFailedToReceiveAd event received with message: " + args.Message);
//     }

//     public void HandleAdOpened (object sender, EventArgs args)
//     {
//         MonoBehaviour.print ("HandleAdOpened event received");
//     }

//     public void HandleAdClosed (object sender, EventArgs args)
//     {
//         MonoBehaviour.print ("HandleAdClosed event received");
//     }

//     public void HandleAdLeftApplication (object sender, EventArgs args)
//     {
//         MonoBehaviour.print ("HandleAdLeftApplication event received");
//     }

//     #endregion
// }