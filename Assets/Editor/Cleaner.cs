﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class Folder
{
    public int id;
    public string name;
}

[System.Serializable]
public class Project
{
    public Folder[] folders;
}

public class Cleaner : MonoBehaviour
{
    [MenuItem ("LumiosGames/Create Folder")]
    static void CreateFolder ()
    {
        string guid = AssetDatabase.CreateFolder ("Assets", "My Folder");
        string newFolderPath = AssetDatabase.GUIDToAssetPath (guid);
    }

    [MenuItem ("LumiosGames/Create all")]
    static void CreateHierarchy ()
    {

        string data = File.ReadAllText ("Assets/Editor/Settings.json");
        Project hier = JsonUtility.FromJson<Project> (data);

        for (int i = 0; i < hier.folders.Length; i++)
        {
            string guid = AssetDatabase.CreateFolder ("Assets", hier.folders[i].name);
            string newFolderPath = AssetDatabase.GUIDToAssetPath (guid);
        }
    }

    [MenuItem ("LumiosGames/Debug")]
    static void ExportHierarchy ()
    {
        foreach (string file in System.IO.Directory.GetFiles ("Assets"))
            Debug.Log(file);
        }
}